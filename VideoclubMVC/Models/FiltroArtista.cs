﻿using DAL.modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utilidades.Formularios;
using Utilidades.LambdaLinq;

namespace VideoclubMVC.Models
{
    public class FiltroArtista
    {
        public List<Artista> Artistas { get; set; }
        public FormModel FormModel { get; set; }
    }
}