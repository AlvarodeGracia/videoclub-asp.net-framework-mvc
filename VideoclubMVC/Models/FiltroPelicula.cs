﻿using DAL.modelos;
using System;
using System.Collections.Generic;
using System.Web;
using Utilidades.Formularios;
using Utilidades.LambdaLinq;

namespace VideoclubMVC.Models
{
    public class FiltroPelicula
    {
    
        public List<Pelicula> Peliculas { get; set; }
        public FormModel FormModel { get; set; }

    }
}
