﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaseDatos;
using DAL.modelos;
using VideoclubMVC.Models;
using Utilidades.LambdaLinq;
using Utilidades.Formularios;

namespace VideoclubMVC.Controllers
{
    public class PeliculasController : Controller
    {
        private DBConect db = new DBConect();

        // GET: Peliculas
        public ActionResult Index()
        {
            FiltroPelicula filtro = new FiltroPelicula();
            filtro.FormModel = new FormModel(typeof(Pelicula));
            filtro.Peliculas = db.Peliculas.ToList();
           
            return View(filtro);
        }


        public ActionResult Filtrar(FormCollection form)
        {
            
            ModeloFiltro<Pelicula> filtro = new ModeloFiltro<Pelicula>();
            FiltroPelicula filtroResult = new FiltroPelicula();
            filtroResult.FormModel = new FormModel(typeof(Pelicula));
            filtroResult.Peliculas = filtro.Filtrar(db.Peliculas.Where(xs => true), form);

            return View("Index", filtroResult);
            
        }

        // GET: Peliculas/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pelicula pelicula = db.Peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        // GET: Peliculas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Peliculas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdPelicula,Titulo,FechaEstreno,Duracion,Genero,Nacionalidad,Nota,Sinopsios")] Pelicula pelicula)
        {
            if (ModelState.IsValid)
            {
                pelicula.IdPelicula = Guid.NewGuid();
                db.Peliculas.Add(pelicula);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pelicula);
        }

        // GET: Peliculas/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pelicula pelicula = db.Peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        // POST: Peliculas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPelicula,Titulo,FechaEstreno,Duracion,Genero,Nacionalidad,Nota,Sinopsios")] Pelicula pelicula)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pelicula).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pelicula);
        }

        // GET: Peliculas/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pelicula pelicula = db.Peliculas.Find(id);
            if (pelicula == null)
            {
                return HttpNotFound();
            }
            return View(pelicula);
        }

        // POST: Peliculas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Pelicula pelicula = db.Peliculas.Find(id);
            db.Peliculas.Remove(pelicula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
