﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaseDatos;
using DAL.modelos;

namespace VideoclubMVC.Controllers
{
    public class ArtistaPeliculasController : Controller
    {
        private DBConect db = new DBConect();

        // GET: ArtistaPeliculas
        public ActionResult Index()
        {
            var artistasPeliculas = db.ArtistasPeliculas.Include(a => a.Artista).Include(a => a.Pelicula);
            return View(artistasPeliculas.ToList());
        }

        // GET: ArtistaPeliculas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArtistaPelicula artistaPelicula = db.ArtistasPeliculas.Find(id);
            if (artistaPelicula == null)
            {
                return HttpNotFound();
            }
            return View(artistaPelicula);
        }

        // GET: ArtistaPeliculas/Create
        public ActionResult Create()
        {
            ViewBag.idArtista = new SelectList(db.Artistas, "id", "Nombre");
            ViewBag.IdPelicula = new SelectList(db.Peliculas, "IdPelicula", "Titulo");
            return View();
        }

        // POST: ArtistaPeliculas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idArtista,IdPelicula,Puesto")] ArtistaPelicula artistaPelicula)
        {
            if (ModelState.IsValid)
            {
                db.ArtistasPeliculas.Add(artistaPelicula);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idArtista = new SelectList(db.Artistas, "id", "Nombre", artistaPelicula.idArtista);
            ViewBag.IdPelicula = new SelectList(db.Peliculas, "IdPelicula", "Titulo", artistaPelicula.IdPelicula);
            return View(artistaPelicula);
        }

        // GET: ArtistaPeliculas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArtistaPelicula artistaPelicula = db.ArtistasPeliculas.Find(id);
            if (artistaPelicula == null)
            {
                return HttpNotFound();
            }
            ViewBag.idArtista = new SelectList(db.Artistas, "id", "Nombre", artistaPelicula.idArtista);
            ViewBag.IdPelicula = new SelectList(db.Peliculas, "IdPelicula", "Titulo", artistaPelicula.IdPelicula);
            return View(artistaPelicula);
        }

        // POST: ArtistaPeliculas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idArtista,IdPelicula,Puesto")] ArtistaPelicula artistaPelicula)
        {
            if (ModelState.IsValid)
            {
                db.Entry(artistaPelicula).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idArtista = new SelectList(db.Artistas, "id", "Nombre", artistaPelicula.idArtista);
            ViewBag.IdPelicula = new SelectList(db.Peliculas, "IdPelicula", "Titulo", artistaPelicula.IdPelicula);
            return View(artistaPelicula);
        }

        // GET: ArtistaPeliculas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ArtistaPelicula artistaPelicula = db.ArtistasPeliculas.Find(id);
            if (artistaPelicula == null)
            {
                return HttpNotFound();
            }
            return View(artistaPelicula);
        }

        // POST: ArtistaPeliculas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ArtistaPelicula artistaPelicula = db.ArtistasPeliculas.Find(id);
            db.ArtistasPeliculas.Remove(artistaPelicula);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
