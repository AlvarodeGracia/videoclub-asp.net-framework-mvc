﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BaseDatos;
using DAL.modelos;
using Utilidades.LambdaLinq;
using VideoclubMVC.Models;
using Utilidades.Formularios;
using PagedList;

namespace VideoclubMVC.Controllers
{
    [Authorize]
    [AllowAnonymous]
    public class ArtistasController : Controller
    {
        private DBConect db = new DBConect();

        // GET: Artistas
        [HttpGet]
        public ActionResult Index(int? page, int? elements)
        {
            var list = db.Artistas.ToList(); 
            var pageNumber = page ?? 1;
            var elementsPerPage = elements ?? 25;
            // will only contain 25 products max because of the pageSiz;
           
            var onePageOfProducts = list.AsQueryable().ToPagedList(pageNumber, elementsPerPage);
            ViewBag.OnePageOfProducts = onePageOfProducts;
            ViewBag.NumeroElementos = elementsPerPage;

            FiltroArtista filtroResult = new FiltroArtista();
            filtroResult.FormModel = new FormModel(typeof(Artista));
            filtroResult.Artistas = list;
            return View(filtroResult);
        }

        public ActionResult Filtrar(FormCollection form)
        {

            ModeloFiltro<Artista> filtro = new ModeloFiltro<Artista>();
            FiltroArtista filtroResult = new FiltroArtista();
            filtroResult.FormModel = new FormModel(typeof(Artista));
            filtroResult.Artistas = filtro.Filtrar(db.Artistas.Where(xs => true), form);

            return View("Index", filtroResult);

        }

        // GET: Artistas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artista artista = db.Artistas.Find(id);
            if (artista == null)
            {
                return HttpNotFound();
            }
            return View(artista);
        }

        // GET: Artistas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Artistas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Nombre,Apellidos,FechaNacimiento,FechaFallecimiento,Nacionalidad")] Artista artista)
        {
            if (ModelState.IsValid)
            {
                db.Artistas.Add(artista);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(artista);
        }

        // GET: Artistas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artista artista = db.Artistas.Find(id);
            if (artista == null)
            {
                return HttpNotFound();
            }
            return View(artista);
        }

        // POST: Artistas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Nombre,Apellidos,FechaNacimiento,FechaFallecimiento,Nacionalidad")] Artista artista)
        {
            if (ModelState.IsValid)
            {
                db.Entry(artista).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(artista);
        }

        // GET: Artistas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Artista artista = db.Artistas.Find(id);
            if (artista == null)
            {
                return HttpNotFound();
            }
            return View(artista);
        }

        // POST: Artistas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Artista artista = db.Artistas.Find(id);
            db.Artistas.Remove(artista);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
