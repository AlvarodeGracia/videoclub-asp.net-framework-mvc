﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Utilidades.LambdaLinq
{
    public class Filtro
    {

        public static Func<T, bool> generarLambdaEquals<T>(String clave, String valor)
        {
            //Obtenemos el tipo de la propiedad buscada.
            var typeClave = typeof(T).GetProperty(clave).PropertyType;

            //Convertimos el valor que nos llega al tipo del modelo.
            var parseValue = TypeDescriptor.GetConverter(typeClave).ConvertFrom(valor);

            ParameterExpression param = Expression.Parameter(typeof(T));

            //Arboles de Expresion
            //Con esto creamos una expresion lamda que usaremos luego en linq
            //Expresion.Lamda<Func<TDelegate>> es el resultado que pasaremos a Linq
            //Expresion lambda tratada - (x => parametro == objetivo)
            //Este pide un Body y un Param
            //Body = Es el equivalente al parametro == Objetivo, en est caso
            //creamos un body para hacer un equals entre dos expresiones
            var condition =
               //Creamos una expresion de tipo Lambda
               Expression.Lambda<Func<T, bool>>(
                   //Equal crea un expresion de comparacion binaria
                   //X.Nombre == "Alvaro"
                   Expression.Equal(
                       //Esta expresion representa el acceso a una propiedad
                       //Por ejemplo x.Nombre
                       Expression.Property(param, clave),
                       //Crea una Exprersion que representa un valor, en este caso "Alvaro"
                       Expression.Constant(parseValue, typeClave)
                   ),
                   //Este es el quivalente al x => que en este caso sera de tipo Pelicula
                   param
               //El compile, compila en tiemp ode ejecucion la expresion lamda y la preapra
               //Como u nTDelegate para pasarsela al LINQ
               ).Compile();

            return condition;

        }

        public static Func<T, bool> generarLambdaMenor<T>(String clave, String valor)
        {
            //Obtenemos el tipo de la propiedad buscada.
            var typeClave = typeof(T).GetProperty(clave).PropertyType;

            //Convertimos el valor que nos llega al tipo del modelo.
            var parseValue = TypeDescriptor.GetConverter(typeClave).ConvertFrom(valor);

            ParameterExpression param = Expression.Parameter(typeof(T));

            //Arboles de Expresion
            //Con esto creamos una expresion lamda que usaremos luego en linq
            //Expresion.Lamda<Func<TDelegate>> es el resultado que pasaremos a Linq
            //Expresion lambda tratada - (x => parametro == objetivo)
            //Este pide un Body y un Param
            //Body = Es el equivalente al parametro == Objetivo, en est caso
            //creamos un body para hacer un equals entre dos expresiones
            var condition =
               //Creamos una expresion de tipo Lambda
               Expression.Lambda<Func<T, bool>>(
                   //Equal crea un expresion de comparacion binaria
                   //X.Nombre == "Alvaro"
                   Expression.LessThan(
                       //Esta expresion representa el acceso a una propiedad
                       //Por ejemplo x.Nombre
                       Expression.Property(param, clave),
                       //Crea una Exprersion que representa un valor, en este caso "Alvaro"
                       Expression.Constant(parseValue, typeClave)
                   ),
                   //Este es el quivalente al x => que en este caso sera de tipo Pelicula
                   param
               //El compile, compila en tiemp ode ejecucion la expresion lamda y la preapra
               //Como u nTDelegate para pasarsela al LINQ
               ).Compile();

            return condition;

        }

        public static Func<T, bool> generarLambdaMayor<T>(String clave, String valor)
        {
            //Obtenemos el tipo de la propiedad buscada.
            var typeClave = typeof(T).GetProperty(clave).PropertyType;

            //Convertimos el valor que nos llega al tipo del modelo.
            var parseValue = TypeDescriptor.GetConverter(typeClave).ConvertFrom(valor);

            ParameterExpression param = Expression.Parameter(typeof(T));

            //Arboles de Expresion
            //Con esto creamos una expresion lamda que usaremos luego en linq
            //Expresion.Lamda<Func<TDelegate>> es el resultado que pasaremos a Linq
            //Expresion lambda tratada - (x => parametro == objetivo)
            //Este pide un Body y un Param
            //Body = Es el equivalente al parametro == Objetivo, en est caso
            //creamos un body para hacer un equals entre dos expresiones
            var condition =
               //Creamos una expresion de tipo Lambda
               Expression.Lambda<Func<T, bool>>(
                   //Equal crea un expresion de comparacion binaria
                   //X.Nombre == "Alvaro"
                   Expression.GreaterThan(
                       //Esta expresion representa el acceso a una propiedad
                       //Por ejemplo x.Nombre
                       Expression.Property(param, clave),
                       //Crea una Exprersion que representa un valor, en este caso "Alvaro"
                       Expression.Constant(parseValue, typeClave)
                   ),
                   //Este es el quivalente al x => que en este caso sera de tipo Pelicula
                   param
               //El compile, compila en tiemp ode ejecucion la expresion lamda y la preapra
               //Como u nTDelegate para pasarsela al LINQ
               ).Compile();

            return condition;

        }

        public static Func<T, bool> generarLambdaEqualEnum<T>(String clave, Object valor)
        {
            //Obtenemos el tipo de la propiedad buscada.
            //var typeClave = typeof(T).GetProperty(clave).PropertyType;

            //Convertimos el valor que nos llega al tipo del modelo.
            //var parseValue = TypeDescriptor.GetConverter(typeClave).ConvertFrom(valor);

            ParameterExpression param = Expression.Parameter(typeof(T));

            //Arboles de Expresion
            //Con esto creamos una expresion lamda que usaremos luego en linq
            //Expresion.Lamda<Func<TDelegate>> es el resultado que pasaremos a Linq
            //Expresion lambda tratada - (x => parametro == objetivo)
            //Este pide un Body y un Param
            //Body = Es el equivalente al parametro == Objetivo, en est caso
            //creamos un body para hacer un equals entre dos expresiones
            var condition =
               //Creamos una expresion de tipo Lambda
               Expression.Lambda<Func<T, bool>>(
                   //Equal crea un expresion de comparacion binaria
                   //X.Nombre == "Alvaro"
                   Expression.Equal(
                       //Esta expresion representa el acceso a una propiedad
                       //Por ejemplo x.Nombre
                       Expression.Property(param, clave),
                       //Crea una Exprersion que representa un valor, en este caso "Alvaro"
                       Expression.Constant(valor, valor.GetType())
                   ),
                   //Este es el quivalente al x => que en este caso sera de tipo Pelicula
                   param
               //El compile, compila en tiemp ode ejecucion la expresion lamda y la preapra
               //Como u nTDelegate para pasarsela al LINQ
               ).Compile();

            return condition;

        }

    }
}
