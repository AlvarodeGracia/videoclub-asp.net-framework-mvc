﻿using DAL.modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Utilidades.Formularios;


/*
 * Este modelo debe ayudar a filtrar cualquier tipo de objeto por rango y por tipo.
 */
namespace Utilidades.LambdaLinq
{
    //Mandar el objeto a un formulario que se crea a partir de los valores de Claves y Tipos
    //Hay que poder pasar un List
    public class ModeloFiltro<T>
    {

        public List<Func<T, bool>> prepararFiltro(FormCollection form)
        {
            var properties = typeof(T).GetProperties().ToList();

            List<Func<T, bool>> funcionesLambda = new List<Func<T, bool>>();

            //Tipo.GetCustomAttribute("asd");

            foreach (PropertyInfo x in properties)
            {   
                //Compobamos si el metodo de esa clase es enum
                if (x.PropertyType.IsEnum)
                {
                    //Obtnemos todos los valores del enum
                    String[] values = Enum.GetNames(x.PropertyType);
                    //Obtenemos el nombre del enum
                    var clave = x.Name;
                    //Obtenemos el valor del formulario obtenido
                    var valor = form.Get(clave);

                    Object enumValor = null;

                    //Recorremos los valores del enum hasta que coincida con
                    //el valor del formulario.
                    foreach (String s in values)
                    {
                        //Si coincide
                        if(valor == s)
                        {
                            //Obtenemos el valor del enum
                            enumValor = Enum.Parse(x.PropertyType, valor);
                           
                        }
                    }

                    if(enumValor != null)
                        funcionesLambda.Add(Filtro.generarLambdaEqualEnum<T>(clave, enumValor));

                }
                else if (IsNumericType(x.PropertyType))
                {
                    var clave = x.Name;
                    var clave1 = x.Name+"+inicio";
                    var valor1 = form.Get(clave1);
                    var clave2 = x.Name+"+fin";
                    var valor2 = form.Get(clave2);

                    if(valor1 != null && valor2 != null && valor1 != "" && valor2 != "")
                    {
                        if (valor1 != null)
                            funcionesLambda.Add(Filtro.generarLambdaMayor<T>(clave, valor1));
                        if (valor2 != null)
                            funcionesLambda.Add(Filtro.generarLambdaMenor<T>(clave, valor2));

                    }else if(valor1 != null && valor1 != "")
                    {
                        funcionesLambda.Add(Filtro.generarLambdaEquals<T>(clave, valor1));
                    }
                    
                }
                else
                {
                    var clave = x.Name;
                    var valor = form.Get(x.Name);
                    if (valor != null && valor != "")
                        funcionesLambda.Add(Filtro.generarLambdaEquals<T>(clave, valor));

                }

            }

            return funcionesLambda;

            
        }

        public bool IsNumericType(Type t)
        {
            switch (Type.GetTypeCode(t))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }


        public List<T> Filtrar(IQueryable<T> query, FormCollection form)
        {
            //List<T> list = new List<T>();

            List<Func<T, bool>> lambdas = prepararFiltro(form);

            foreach(Func<T, bool> func in lambdas)
            {

                query = query.Where(func).AsQueryable();
            }

            var lista = query.ToList();

            return lista;
        }

    }

  



    //Enum de los tipos de filtro que tenemos
    public enum TiposFiltro
    {
        EQUALS = 1,
        RANGE = 2
    }
}
