﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Utilidades.Formularios
{
    public class FormModel
    {
        public List<FormImput> Imputs { get; set; }

        //Nombre de los campos del modelo
        public List<String> claves { get; set; }

        //Tipo del modelo
        public Type Tipo { get; set; }

        public FormModel(Type tipo)
        {
            Imputs = new List<FormImput>();
            Tipo = tipo;
            iniciarClaves();
            
        }

        private void iniciarClaves()
        {
            claves = new List<String>();

            var properties = Tipo.GetProperties().ToList();
            
            foreach (PropertyInfo x in properties)
            {

                FormImput imput = new FormImput() { Name = x.Name, Value = "", Tipo = "text" };

                claves.Add(x.Name);

                if (x.PropertyType.IsEnum)
                {
                    imput.Tipo = "enum";
                    List<SelectListItem> lista = new List<SelectListItem>();

                    String[] values = Enum.GetNames(x.PropertyType);

                    SelectListItem itemAux = new SelectListItem();
                    itemAux.Value = "none";
                    itemAux.Text = "";
                    lista.Add(itemAux);

                    foreach (String name in values)
                    {
                        SelectListItem item = new SelectListItem();
                        item.Value = name;
                        item.Text = name;
                        lista.Add(item);
                    }

                    imput.Lista = lista;

                }
                else if (IsNumericType(x.PropertyType))
                {
                    imput.Tipo = "range";
                }

                Imputs.Add(imput);
            }
        }

        public bool IsNumericType(Type t)
        {
            switch (Type.GetTypeCode(t))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }


    }


}
