﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Utilidades.Formularios
{
    
    public class FormImput
    {

        //public static const String IMPUT_TEXT = "IMPUT_TEXT";

        public String Name { get; set; }
        public String Value { get; set; }
        public String Tipo { get; set; }
        public List<SelectListItem> Lista { get; set; }

        public FormImput()
        {
            Lista = new List<SelectListItem>();
        }
    }
}
