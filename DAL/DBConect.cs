﻿using DAL.modelos;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace BaseDatos
{
   
    public class DBConect: IdentityDbContext<ApplicationUser>
    {

        public DBConect()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.CommandTimeout = 50000;
            Database.SetInitializer(new InicializadorDbContext());
        }

        //public DBConect() : base("DefaultConnection")
        //{
        //    Database.CommandTimeout = 50000;
        //    Database.SetInitializer(new InicializadorDbContext());
        //}

        public DBConect(string connectionString) : base(connectionString)
        {
            Database.CommandTimeout = 50000;
            Database.SetInitializer(new InicializadorDbContext());
        }      

        public static DBConect Create()
        {
            return new DBConect();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Pelicula>().Property(pedido => pedido.IdPelicula)
                   .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }

        public System.Data.Entity.DbSet<Artista> Artistas { get; set; }
        public System.Data.Entity.DbSet<Pelicula> Peliculas { get; set; }
        public System.Data.Entity.DbSet<ArtistaPelicula> ArtistasPeliculas { get; set; } 

        public class InicializadorDbContext : CreateDatabaseIfNotExists<DBConect>
        {
            protected override void Seed(DBConect context)
            {
                base.Seed(context);
            }
        }
    }

    // Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    public class ApplicationUser : IdentityUser
    {

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

}
