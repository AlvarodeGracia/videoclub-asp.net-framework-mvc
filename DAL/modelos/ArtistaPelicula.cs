﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.modelos
{
    [Table("ArtistaPelicula")]
    public class ArtistaPelicula
    {

        [Key, Column(Order = 1), ForeignKey("Artista")]
        public int idArtista { get; set; }
        public virtual Artista Artista { get; set; }

        [Key, Column(Order = 2), ForeignKey("Pelicula")]
        public Guid IdPelicula { get; set; }
        public virtual Pelicula Pelicula { get; set; }

        public PuestoArtista Puesto { get; set; }

    }

    public enum PuestoArtista
    {
        ACTOR = 1,
        DIRECTOR = 2
    }
}
