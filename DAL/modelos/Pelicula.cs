﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.modelos
{
    [Table("Peliculas")]
    public class Pelicula
    {
        [Key]
        public Guid IdPelicula { get; set; }
        [Required]
        public String Titulo { get; set; }
        [Required]
        public DateTime FechaEstreno { get; set; }
        public int Duracion { get; set; }
        public Genero Genero { get; set; }
        public String Nacionalidad { get; set; }
        public int Nota { get; set; }
        public String Sinopsios { get; set; }

    }

    public enum Genero
    {
        TERROR = 1,
        ACCION = 2,
        AVENTURAS = 4
    }
}
