﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.modelos
{
    [Table("Artistas")]
    public class Artista
    {
        [Key]
        public int id { get; set; }
        [Required]
        public String Nombre { get; set; }
        [Required]
        public String Apellidos { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public DateTime FechaFallecimiento { get; set; }
        public String Nacionalidad { get; set; }

    }

}

